﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AbyssalChicken.Behaviour.EmotionSystem
{
    public class EmotionalStatemachine 
    {
        // SINGLETON
        private static EmotionalStatemachine _instance;
        public static EmotionalStatemachine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EmotionalStatemachine();
                }
                return _instance;
            }
        }


        List<Emotion> _emotions;

        private class Emotion
        {
            public EmotionalState emotionState;
            public float threshold;
            public float currentValue;
        }
        
        private EmotionalState _currentEmotion;




        public void AddToEmotinalValue(EmotionalState emotion, float value)
        {
            _emotions.First(x => x.emotionState == emotion).threshold += value;
        }


    }
}