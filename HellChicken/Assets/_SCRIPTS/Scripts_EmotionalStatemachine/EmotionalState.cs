﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbyssalChicken.Behaviour.EmotionSystem
{
    public enum EmotionalState
    {
        Neutral,
        Curious,
        Bored,
        Sleepy,
        Angry,
        Hungry,
        Scared
    }
}