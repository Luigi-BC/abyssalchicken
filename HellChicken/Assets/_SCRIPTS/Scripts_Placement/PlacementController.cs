﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlacementController : MonoBehaviour
{
    [SerializeField] private GameObject _placementPrefab;
#if UNITY_EDITOR
    [SerializeField] private GameObject _editorCamera;
#endif
    private ARRaycastManager _arRaycastManager;
    private bool _isplaced;

    private void Awake()
    {
        _arRaycastManager = GetComponent<ARRaycastManager>();
#if UNITY_EDITOR
        Destroy(transform.GetChild(0).gameObject);
        Instantiate(_editorCamera);
#endif
    }

    private bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if(Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
        touchPosition = Vector2.zero;
        return false;
    }

    private void Update()
    {
        //disable script if is placed.
        if (_isplaced)
        {
            enabled = false;
            return;
        }


#if UNITY_EDITOR

        Instantiate(_placementPrefab, Vector3.zero, Quaternion.identity);
        // Instantiate editor camera

        _isplaced = true;
        if (Application.isPlaying)
        {
            return;
        }

#endif

        //Try to get touch input
        if (!TryGetTouchPosition(out Vector2 touchPosition))
        {
            return;
        }

        //get the hitposition (if there is any) and place the object
        if(_arRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;
            Instantiate(_placementPrefab, hitPose.position, hitPose.rotation);
            _isplaced = true;
        }
    }
    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();
}
