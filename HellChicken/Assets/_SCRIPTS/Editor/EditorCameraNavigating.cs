﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorCameraNavigating : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _rotationSpeed;

    private float xRotation;
    private float yRotation;

    // Update is called once per frame
    void Update()
    {

        //ROTATION
        xRotation = -Input.GetAxis("Mouse Y") * Time.deltaTime * _rotationSpeed;
        yRotation = Input.GetAxis("Mouse X") * Time.deltaTime * _rotationSpeed;

        Vector3 euler = transform.eulerAngles;
        euler.y += yRotation;
        euler.x += xRotation;
        transform.eulerAngles = euler;

        //MOVEMENT
        Vector3 moveVector = Vector3.zero;

        moveVector.x += GetLeftRightMovement();
        moveVector.y += GetUpDownMovement();
        moveVector.z += GetForwardBackwardsMovement();

        transform.position += transform.rotation *  moveVector;
    }

    private float GetLeftRightMovement()
    {
        return Input.GetAxis("Horizontal") * Time.deltaTime * _moveSpeed;
    }

    private float GetForwardBackwardsMovement()
    {
        return Input.GetAxis("Vertical") * Time.deltaTime * _moveSpeed;
    }

    private float GetUpDownMovement()
    {
        float upDown = 0f;
        upDown += Input.GetKey(KeyCode.E) ? 1f : 0f;
        upDown -= Input.GetKey(KeyCode.Q) ? 1f : 0f;

        return upDown * Time.deltaTime * _moveSpeed;

    }

}
