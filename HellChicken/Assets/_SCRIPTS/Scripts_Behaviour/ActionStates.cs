﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbyssalChicken.Behaviour
{
    public class BehaviourAction 
    {

    }

    public enum ActionState
    {
        FloorScratching,
        PortalJumping,
        WingFluttering,
        FloorPecking,
        Screaming,
        Resting,
        Drooling,
        FlyingAway,
        FlyingBack,
        Eating,
        WalkingAround,
        RunningAround,
        HidingFromCamera
    }

}