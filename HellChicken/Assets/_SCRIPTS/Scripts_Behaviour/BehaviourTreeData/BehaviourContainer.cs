﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class BehaviourContainer : ScriptableObject
    {
        public List<BehaviourNodeData> behaviourNodes = new List<BehaviourNodeData>();
    }
}
