﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbyssalChicken.Behaviour
{
    public enum NodeType
    {
        Root,
        Selector,
        Sequence,
        EmotionalCondition,
        ActionNode,
        Decorator
    }

    [System.Serializable]
    public class BehaviourNodeData
    {
        public string guid;
        public NodeType nodeType;
        public string contentAsJSON;
        public List<string> childGuids;
        public CustomRect rect;
    }

    [System.Serializable]
    public struct CustomRect
    {
        public float x, y, width, height;

        public static implicit operator Rect(CustomRect r)
        {
            return new Rect(r.x, r.y, r.width, r.height);
        }

        public CustomRect(Rect rect)
        {
            x = rect.x;
            y = rect.y;
            width = rect.width;
            height = rect.height;
        }
    }
}