﻿using AbyssalChicken.Behaviour;
using AbyssalChicken.Behaviour.EmotionSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BehaviourTest : MonoBehaviour
{
    public static BehaviourTest instance;

    public EmotionalState currentEmotion;
    private ActionState _currentAction;
    public ActionState Currentaction
    {
        get => _currentAction;
        set
        {
            if(_currentAction == value)
            {
                return;
            }
            _currentAction = value;
            UpdateAction();
        }
    }

    [SerializeField] private TextMeshProUGUI _actionResultText;

    private void Awake()
    {
        instance = this;
    }


    public void OnBtnEmotionClick(int emotionIndex)
    {
        currentEmotion = (EmotionalState)emotionIndex;
    }

    

    private void UpdateAction()
    {
        _actionResultText.text = "Action: " + _currentAction.ToString();
    }
}
