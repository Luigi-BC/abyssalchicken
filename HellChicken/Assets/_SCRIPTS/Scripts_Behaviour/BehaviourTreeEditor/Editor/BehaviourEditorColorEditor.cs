﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;

namespace AbyssalChicken.Behaviour.EditorGraph
{
    public class BehaviourEditorColorEditor : EditorWindow
    {

        private static BehaviourGraphWindow _graphWindow;


        public static void OpenWindow(BehaviourGraphWindow graphWindow)
        {
            BehaviourEditorColorEditor colorEditor = GetWindow<BehaviourEditorColorEditor>("Color Editor");

            _graphWindow = graphWindow;

            colorEditor.Show();
        }



        private void OnGUI()
        {
            BehaviourEditorColorScheme colorScheme = _graphWindow.currentColorScheme;


            GUIStyle headerStyle = new GUIStyle(GUI.skin.label) { fontStyle = FontStyle.Bold};

            EditorGUI.BeginChangeCheck();

            GUILayout.Space(10);
            EditorGUILayout.LabelField("Background", headerStyle);

            colorScheme.background = EditorGUILayout.ColorField("Background Color", colorScheme.background);



            GUILayout.Space(10);
            EditorGUILayout.LabelField("Lines", headerStyle);

            colorScheme.nodeLines = EditorGUILayout.ColorField("Line Color", colorScheme.nodeLines);



            GUILayout.Space(10);
            EditorGUILayout.LabelField("Node Status (Runtime)", headerStyle);

            colorScheme.nodeSuccessful = EditorGUILayout.ColorField("Successful", colorScheme.nodeSuccessful);
            colorScheme.nodeFailed = EditorGUILayout.ColorField("Failed", colorScheme.nodeFailed);
            colorScheme.nodeInactive = EditorGUILayout.ColorField("Inactive", colorScheme.nodeInactive);
            colorScheme.nodeRunning = EditorGUILayout.ColorField("Running", colorScheme.nodeRunning);


            GUILayout.Space(10);
            EditorGUILayout.LabelField("Font Colors", headerStyle);

            colorScheme.fontColor = EditorGUILayout.ColorField("Font Color", colorScheme.fontColor);


            GUILayout.Space(10);
            EditorGUILayout.LabelField("Node Colors", headerStyle);

            colorScheme.baseNodeColor = EditorGUILayout.ColorField("Base Color", colorScheme.baseNodeColor);
            colorScheme.rootNode = EditorGUILayout.ColorField("Root Node", colorScheme.rootNode);
            colorScheme.selectorNode = EditorGUILayout.ColorField("Selector Node", colorScheme.selectorNode);
            colorScheme.sequenceNode = EditorGUILayout.ColorField("Sequence Node", colorScheme.sequenceNode);
            colorScheme.actionNode = EditorGUILayout.ColorField("Action Node", colorScheme.actionNode);
            colorScheme.emotionalNode = EditorGUILayout.ColorField("Emotional Node", colorScheme.emotionalNode);
            colorScheme.decoratorNode = EditorGUILayout.ColorField("Decorator Node", colorScheme.decoratorNode);

            if(GUILayout.Button("Save Color Scheme"))
            {
                _graphWindow.SaveColorScheme();
            }

            if (EditorGUI.EndChangeCheck())
            {
                _graphWindow.UpdateColors();
            }

        }
    }
}