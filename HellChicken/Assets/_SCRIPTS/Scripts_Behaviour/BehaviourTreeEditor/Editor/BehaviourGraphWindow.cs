﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Graphs;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace AbyssalChicken.Behaviour.EditorGraph
{
    /// <summary>
    /// Editor window for the Chicken Behaviour Graph Editor.
    /// </summary>
    public class BehaviourGraphWindow : EditorWindow
    {
        private BehaviourGraphView _graphView;
        private BehaviourGraphView GraphView
        {
            get { return _graphView; }
            set
            {
                _graphView = value;
            }
        }

        private BehaviourEditorColorSchemeHolderSO _colorHolderSO;
        public BehaviourEditorColorSchemeHolderSO ColorHolderSO
        {
            get
            {
                if (_colorHolderSO == null)
                {
                    _colorHolderSO = Resources.Load<BehaviourEditorColorSchemeHolderSO>("BehaviourEditorColorSchemeHolderSO");
                }
                return _colorHolderSO;
            }
        }



        public enum ColorTheme { Light, Dark };
        private ColorTheme _currentColorTheme;
        public ColorTheme CurrentColorTheme
        {
            get { return _currentColorTheme; }
            private set
            {
                _currentColorTheme = value;

                switch (value)
                {
                    case ColorTheme.Light:
                        currentColorScheme = ColorHolderSO.lightTheme;
                        break;
                    case ColorTheme.Dark:
                        currentColorScheme = ColorHolderSO.darkTheme;
                        break;
                }

                UpdateColors();
            }
        }
        public BehaviourEditorColorScheme currentColorScheme;

        public bool initialized;


        [MenuItem("Abyssal Chicken/Behaviour Graph")]
        public static void OpenBehaviourGraphWindow()
        {
            var window = GetWindow<BehaviourGraphWindow>("Behaviour Graph", true, typeof(SceneView));


            //window.CreateGraphView();
            //window.CreateToolBar();
            window.LoadTemporaryGraph();
            window.initialized = true;

            window.UpdateColors();

            AssemblyReloadEvents.afterAssemblyReload += window.LoadTemporaryGraph;
        }

        private void OnEnable()
        {
            if (initialized && !EditorApplication.isPlayingOrWillChangePlaymode)
            {
                LoadTemporaryGraph();
            }
            else if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                CreateGraphView();
                CreateToolBar();
            }
        }

        public void SaveColorScheme()
        {
            EditorUtility.SetDirty(ColorHolderSO);
        }

        private void OnLostFocus()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                SaveTemporaryGraph();
            }
        }

        private void SaveTemporaryGraph()
        {
            GraphSaveUtility.Instance(GraphView).SaveGraph(true);
        }

        private void LoadTemporaryGraph()
        {
            CreateGraphView();
            CreateToolBar();
            GraphSaveUtility.Instance(GraphView).LoadGraph(true);
        }

        private void SaveGraphToSO()
        {
            GraphSaveUtility.Instance(GraphView).SaveGraph(false);
        }

        private void LoadGraphFromSO()
        {
            GraphSaveUtility.Instance(GraphView).LoadGraph(false);
        }


        private void CreateGraphView()
        {
            Debug.Log("Creating new graph view");
            GraphView = new BehaviourGraphView
            {
                name = "Behaviour Graph"
            };
            GraphView.EventRequestColorUpdate += UpdateColors;
            GraphView.StretchToParentSize();
            rootVisualElement.Add(GraphView);
            rootVisualElement.styleSheets.Add((StyleSheet)AssetDatabase.LoadAssetAtPath("Assets/_SCRIPTS/Scripts_Behaviour/BehaviourTreeEditor/Editor/BehaviourTreeEditorSkin.uss", typeof(StyleSheet)));
            
            // Apply Theme
            ApplyThemeFromEditorPrefs();
        }

        
        private void ApplyThemeFromEditorPrefs()
        {
            string themeKey = EditorPrefs.GetString(BehaviourTreeVisualData.THEME_KEY);

            switch (themeKey)
            {
                case BehaviourTreeVisualData.DARKTHEME:
                    Debug.Log("Applying dark theme");
                    CurrentColorTheme = ColorTheme.Dark;
                    break;
                case BehaviourTreeVisualData.LIGHTTHEME:
                    CurrentColorTheme = ColorTheme.Light;
                    Debug.Log("Applying light theme");
                    break;
            }
        }


        private void CreateToolBar()
        {
            Debug.Log("Creating Toolbar");

            var toolbar = new Toolbar();

            //SAVE BUTTON
            var saveButton = new Button(() =>
            {
                SaveGraphToSO();
            });
            AddToolBarClassToButton(saveButton);
            saveButton.text = "Save";
            toolbar.Add(saveButton);

            // LOAD BUTTON
            var loadButton = new Button(() =>
            {
                LoadGraphFromSO();
            });
            loadButton.text = "Load";
            toolbar.Add(loadButton);

            //DEBUG BUTTON
            var debugButton = new Button(() =>
            {
                if (EditorApplication.isPlaying)
                {
                    //CreateGraphView();
                    LoadGraphFromSO();
                    GraphView.EnableDebugView();
                }
                else
                {
                    EditorUtility.DisplayDialog("Error", "You can only debug while in Playmode.", "OK");
                }
            });
            debugButton.text = "Debug View";
            toolbar.Add(debugButton);


            // THEME TOGGLE
            var themeToggle = new Toggle();
            themeToggle.RegisterValueChangedCallback(OnThemeToggleChanged);
            themeToggle.SetValueWithoutNotify(EditorPrefs.GetString(BehaviourTreeVisualData.THEME_KEY) == BehaviourTreeVisualData.LIGHTTHEME);
            toolbar.Add(themeToggle);


            var colorEditorButton = new Button(() =>
            {
                OpenColorEditor();
            });
            colorEditorButton.text = "Edit Colors";
            toolbar.Add(colorEditorButton);

            
            foreach(VisualElement element in toolbar.Children())
            {
                element.AddToClassList("toolbarButton");
            }
            rootVisualElement.Add(toolbar);
        }

        private void AddToolBarClassToButton(Button button)
        {
            button.AddToClassList("toolbarButton");
        }

        private void OpenColorEditor()
        {
            BehaviourEditorColorEditor.OpenWindow(this);
        }

        private void OnThemeToggleChanged(ChangeEvent<bool> evt)
        {
            CurrentColorTheme = evt.newValue ? ColorTheme.Light : ColorTheme.Dark;
            EditorPrefs.SetString(BehaviourTreeVisualData.THEME_KEY, evt.newValue ? BehaviourTreeVisualData.LIGHTTHEME : BehaviourTreeVisualData.DARKTHEME);
        }

        public void UpdateColors()
        {
            _graphView.UpdateColors(currentColorScheme);
            Repaint();
        }
    }
}