﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbyssalChicken.Behaviour.EditorGraph
{
    public static class BehaviourTreeVisualData
    {
        public const string EMOTION = "Emotion";
        public const string ACTION = "Action";
        public const string SELECTOR = "Selector";
        public const string SEQUENCE = "Sequence";
        public const string DECORATOR = "Decorator";

        public const string ROOT = "Root";

        //THEME
        public const string THEME_KEY = "BehaviourTreeTheme";
        public const string DARKTHEME = "DARKTHEME";
        public const string LIGHTTHEME = "LIGHTTHEME";
    }
}