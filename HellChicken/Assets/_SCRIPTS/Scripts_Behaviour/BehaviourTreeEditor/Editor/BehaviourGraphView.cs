﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using AbyssalChicken.Behaviour;
using NUnit.Framework.Interfaces;


namespace AbyssalChicken.Behaviour.EditorGraph
{
    public class BehaviourGraphView : GraphView
    {
        //public readonly Vector2 defaultNodeSize = new Vector2(800, 200);
        public Action EventRequestColorUpdate = delegate { };

        private VisualElement backgroundElement;
        
        private Node _contextMenuNode;

        public BehaviourGraphView()
        {
            SetupZoom(0.1f, 5f);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            
            //Background
            backgroundElement = new VisualElement();
            Insert(0, backgroundElement);
            backgroundElement.StretchToParentSize();
            
            // Add event for mouse clicks (context menu)
            RegisterCallback<MouseUpEvent>(OnMouseUp);
            RegisterCallback<MouseDownEvent>(OnMouseDown);

            AddElement(CreateNode<RootGraphNode>(BehaviourTreeVisualData.ROOT));
        }

        public T CreateNode<T>(string nodeName) where T : BehaviourGraphBaseNodeUnspecified, new() 
        {
            T node = new T()
            {
                title = nodeName,
                name = nodeName,
                guid = Guid.NewGuid().ToString()
            };
            
            node.Children().ToList()[0].name = "nodeBorder";
            node.Children().ToList()[1].name = "nodeSelectionBorder";
            node.Initialize();
            node.EventRequestColorRefresh += EventRequestColorUpdate;

            AddElement(node);

            // TODO: Node needs to get the correct position

            EventRequestColorUpdate();
            
            return node;
        }

        public void EnableDebugView()
        {
            List<BehaviourGraphBaseNodeUnspecified> graphNodes = nodes.ToList().Cast<BehaviourGraphBaseNodeUnspecified>().ToList();

            List<BehaviourNode> treeNodes = ChickenBehaviourTree.Instance.allNodes;

            foreach(BehaviourGraphBaseNodeUnspecified node in graphNodes)
            {
                node.LinkGraphNodeToTreeNode(treeNodes.First(x => x.guid == node.guid));
            }
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();

            ports.ForEach((port) =>
            {
                if (startPort != port && startPort.node != port.node)
                {
                    compatiblePorts.Add(port);
                }
            });

            return compatiblePorts;
        }

        public void UpdateColors(in BehaviourEditorColorScheme currentColorScheme)
        {
            List<BehaviourGraphBaseNodeUnspecified> behaviourNodes = nodes.ToList().Where(x => x is BehaviourGraphBaseNodeUnspecified).Cast<BehaviourGraphBaseNodeUnspecified>().ToList();

            //Background
            backgroundElement.style.backgroundColor = currentColorScheme.background;

            //Node Colors
            foreach (BehaviourGraphBaseNodeUnspecified node in behaviourNodes)
            {
                Color nodeColor = Color.white;
                switch (node.nodeType)
                {
                    case NodeType.Root:
                        nodeColor = currentColorScheme.rootNode;
                        break;
                    case NodeType.Selector:
                        nodeColor = currentColorScheme.selectorNode;
                        break;
                    case NodeType.Sequence:
                        nodeColor = currentColorScheme.sequenceNode;
                        break;
                    case NodeType.EmotionalCondition:
                        nodeColor = currentColorScheme.emotionalNode;
                        break;
                    case NodeType.ActionNode:
                        nodeColor = currentColorScheme.actionNode;
                        break;
                    case NodeType.Decorator:
                        nodeColor = currentColorScheme.decoratorNode;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }


                node.titleContainer.style.backgroundColor = nodeColor;
                node.inputContainer.style.backgroundColor = currentColorScheme.baseNodeColor;
                node.outputContainer.style.backgroundColor = currentColorScheme.baseNodeColor;
                node.SetEnumFieldColor(currentColorScheme.baseNodeColor, currentColorScheme.fontColor);
                

                //Font Colors
                Color fontColor = currentColorScheme.fontColor;
                node.titleContainer.Children().ToList().ForEach((x) => x.style.color = fontColor);


                // Port colors
                var portColor = currentColorScheme.nodeLines;

                node.outputPorts.ForEach(x => ColorPort(x, portColor, fontColor));

                if(node.inputPort != null) // root node has no input
                {
                    ColorPort(node.inputPort, portColor, fontColor);
                }

                node.RefreshPorts();
            }


            // Updates the color of the edge
            foreach (Edge edge in edges.ToList())
            {
                edge.HitTest(Vector2.zero);
            }
        }

        public void ColorPort(Port port, Color newPortColor, Color fontColor)
        {
            //Portcolor
            port.portColor = newPortColor;
            VisualElement portChild = port.Children().ToList()[0];
            portChild.style.borderBottomColor = newPortColor;
            portChild.style.borderLeftColor= newPortColor;
            portChild.style.borderTopColor = newPortColor;
            portChild.style.borderRightColor = newPortColor;

            //font color
            port.Children().ToList().ForEach(x => x.style.color = fontColor);
            
            port.MarkDirtyRepaint();
            portChild.MarkDirtyRepaint();
        }

        /// <summary>
        /// Build Rightclick menu 
        /// </summary>
        private void OnMouseUp(MouseUpEvent mouseEvent)
        {
            RemoveContextMenuIfOpen();
            
            if (mouseEvent.button != (int) MouseButton.RightMouse)
                return;

            
            _contextMenuNode = new Node();
            AddElement(_contextMenuNode);
            
            _contextMenuNode.outputContainer.Add(new TextElement(){text = "Create"});
            
            _contextMenuNode.titleContainer.visible = false;
            _contextMenuNode.titleContainer.style.height = 0f;
            //Remove node selection border
            _contextMenuNode.Children().ToList()[1].RemoveFromHierarchy();

            Vector2 localMousePos = ViewportPositionToGraphViewPosition(mouseEvent.localMousePosition);
            
            //Create normal node
            CreateContextMenuButton<ActionGraphNode>(BehaviourTreeVisualData.ACTION, localMousePos);

            //Create selector node
            CreateContextMenuButton<ConditionGraphNode_Emotion>(BehaviourTreeVisualData.EMOTION, localMousePos);

            //Create selector node
            CreateContextMenuButton<SelectorGraphNode>(BehaviourTreeVisualData.SELECTOR, localMousePos);

            //Create selector node
            CreateContextMenuButton<SequenceGraphNode>(BehaviourTreeVisualData.SEQUENCE, localMousePos);
            
            //Create decorator node 
            CreateContextMenuButton<DecoratorGraphNode>(BehaviourTreeVisualData.DECORATOR, localMousePos);
            
            _contextMenuNode.SetPosition(new Rect(localMousePos.x, localMousePos.y, 100, 100));
            _contextMenuNode.name = "ContextMenuNode";
            _contextMenuNode.outputContainer.style.backgroundColor = new StyleColor(Color.clear);
            _contextMenuNode.Children().First().name = "ContextMenuBorder";
            
            
            // --------- LOCAL CREATE BUTTON FUNCTION ------------
            void CreateContextMenuButton<TNodeType>(string nodeTitle, Vector2 mousePosition) where TNodeType : BehaviourGraphBaseNodeUnspecified, new()
            {
                var nodeCreateButton = new Button(() =>
                {
                    Node node = CreateNode<TNodeType>(nodeTitle);
                    node.SetPosition(new Rect(mousePosition.x, mousePosition.y, 300, 300));
                    RemoveContextMenuIfOpen();
                }); 
                nodeCreateButton.name = "ContextMenuButton";
                nodeCreateButton.text = nodeTitle + " Node";
                _contextMenuNode.outputContainer.Add(nodeCreateButton);
            }
        }

        /// <summary>
        /// Transforms the Position of the viewport to the position in the graph view
        /// </summary>
        private Vector2 ViewportPositionToGraphViewPosition(Vector2 position)
        {
            //Scale factor
            float scaleFactor = contentViewContainer.worldBound.width / contentViewContainer.layout.width;
            // for some reason we need to add those (1, 19), the contentviewContainer has this as a default offset.
            return (position - (contentViewContainer.worldBound.position- new Vector2(1f, 19f))) / scaleFactor;
        }

        private void OnMouseDown(MouseDownEvent mouseEvent)
        {
            //Remove context menu on mouse down if it's open
            //TODO: For some reason this does not work with the middle mouse button??
            RemoveContextMenuIfOpen();
        }

        private void RemoveContextMenuIfOpen()
        {
            //Remove context node if it's active
            if (_contextMenuNode != null && nodes.ToList().Contains(_contextMenuNode))
            {
                _contextMenuNode.RemoveFromHierarchy();
            }
        }
    }
}
