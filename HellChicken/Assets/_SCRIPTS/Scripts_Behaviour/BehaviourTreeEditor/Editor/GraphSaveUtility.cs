﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using System.IO;

namespace AbyssalChicken.Behaviour.EditorGraph
{
    /// <summary>
    /// Utility class for saving the behaviour graph.
    /// </summary>
    public class GraphSaveUtility
    {

        private BehaviourGraphView _targetGraph;

        private List<Edge> _edges => _targetGraph.edges.ToList();
        private List<BehaviourGraphBaseNodeUnspecified> _nodes => _targetGraph.nodes.ToList().Where(x => x is BehaviourGraphBaseNodeUnspecified).Cast<BehaviourGraphBaseNodeUnspecified>().ToList();

         
        // Instance 
        public static GraphSaveUtility Instance(BehaviourGraphView targetTraphView)
        {
            return new GraphSaveUtility
            {
                _targetGraph = targetTraphView
            };
        }
        


        /// <summary>
        /// Saves the graph to a ScriptableObject.
        /// </summary>
        public void SaveGraph(bool temporary)
        {
            // If there are no edges throw error and don't save graph.
            if (!_edges.Any())
            {
                Debug.LogError("No Edges!!");
                return;
            }

            // Create new Scriptableobject
            BehaviourContainer behaviourContainer = ScriptableObject.CreateInstance<BehaviourContainer>();
           
            
            // Save all nodes (edges are not saved, only the children nodes)
            foreach(var behaviourNode in _nodes)
            {
                behaviourContainer.behaviourNodes.Add(new BehaviourNodeData
                {
                    guid = behaviourNode.guid,
                    contentAsJSON = behaviourNode.GetContentAsJSON(),
                    nodeType = behaviourNode.nodeType,
                    rect = new CustomRect(behaviourNode.GetPosition()),
                    childGuids = GetChildGUIDs(behaviourNode),
                });
            } 


            if (temporary)
            {
                string path = Application.dataPath + "/Resources/.temp/TempBehaviour.txt";
                FileStream stream = new FileStream(path,FileMode.OpenOrCreate);
                BinaryFormatter bf = new BinaryFormatter();

                bf.Serialize(stream, behaviourContainer.behaviourNodes);

                stream.Close();
            }
            else
            {
                //Create and save asset
                if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                {
                    AssetDatabase.CreateFolder("Assets", "Resources");
                }

                AssetDatabase.CreateAsset(behaviourContainer, "Assets/Resources/ChickenBehaviour.asset");
                EditorUtility.SetDirty(behaviourContainer);
                AssetDatabase.SaveAssets();
            }
        }

        public void LoadGraph(bool temporary)
        {
            BehaviourContainer behaviourContainer = null;

            if (temporary)
            {
                string path = Application.dataPath + "/Resources/.temp/TempBehaviour.txt";
                if (!File.Exists(path))
                {
                    Debug.LogError("File doesnt exist");
                    return;
                }
                FileStream stream = new FileStream(path, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();

                behaviourContainer = ScriptableObject.CreateInstance<BehaviourContainer>();
                behaviourContainer.behaviourNodes = (List<BehaviourNodeData>)bf.Deserialize(stream);

                stream.Close();
                
            }
            else
            {
                behaviourContainer = Resources.Load<BehaviourContainer>("ChickenBehaviour");
            }

            if(behaviourContainer == null)
            {
                Debug.LogError("No asset found!");
                return;
            }

            ClearGraph();
            CreateNodes(behaviourContainer);
            CreateConnections(behaviourContainer);
        }

        private List<string> GetChildGUIDs(BehaviourGraphBaseNodeUnspecified behaviourNode)
        {
            List<string> childNodeGUIDS = new List<string>();


            for(int i = 0; i < behaviourNode.outputPorts.Count; i++)
            {
                if(behaviourNode.outputPorts[i].connections.Count() == 0)
                {
                    continue;
                }
                Node connectedNode = (behaviourNode.outputPorts[i].connections.First().input.node);
                BehaviourGraphBaseNodeUnspecified unspecifiedNode = (BehaviourGraphBaseNodeUnspecified)connectedNode;
                childNodeGUIDS.Add(unspecifiedNode.guid);
            }

            return childNodeGUIDS;
        }


        private void LinkNodes(Port outputPort, Port inputPort)
        {
            var tempEdge = new Edge
            {
                output = outputPort,
                input = inputPort
            };
        
            tempEdge?.input.Connect(tempEdge);
            tempEdge?.output.Connect(tempEdge);
            _targetGraph.Add(tempEdge);
        }

        private void CreateNodes(BehaviourContainer container)
        {
            foreach(var nodeData in container.behaviourNodes)
            {
                // TODO: REFACTOR THIS SO IT DOESN'T NEED A SWITCH STATEMENT!!!
                switch (nodeData.nodeType)
                {
                    case NodeType.Root:
                        var tempRootNode = _targetGraph.CreateNode<RootGraphNode>(BehaviourTreeVisualData.ROOT);
                        AssignDataToNode(tempRootNode, nodeData);
                        break;
                    case NodeType.Selector:
                        var tempSelectorNode = _targetGraph.CreateNode<SelectorGraphNode>(BehaviourTreeVisualData.SELECTOR);
                        AssignDataToNode(tempSelectorNode, nodeData);
                        break;
                    case NodeType.Sequence:
                        var tempSequenceNode = _targetGraph.CreateNode<SequenceGraphNode>(BehaviourTreeVisualData.SEQUENCE);
                        AssignDataToNode(tempSequenceNode, nodeData);
                        break;
                    case NodeType.EmotionalCondition:
                        var tempEmotionalNode = _targetGraph.CreateNode<ConditionGraphNode_Emotion>(BehaviourTreeVisualData.EMOTION);
                        AssignDataToNode(tempEmotionalNode, nodeData);
                        break;
                    case NodeType.ActionNode:
                        var tempActionNode = _targetGraph.CreateNode<ActionGraphNode>(BehaviourTreeVisualData.ACTION);
                        AssignDataToNode(tempActionNode, nodeData);
                        break;
                    case NodeType.Decorator:
                        var tempDecoratorNode = _targetGraph.CreateNode<DecoratorGraphNode>(BehaviourTreeVisualData.DECORATOR);
                        AssignDataToNode(tempDecoratorNode, nodeData);
                        break;
                }
            }
        }

        private void CreateConnections(BehaviourContainer behaviourContainer)
        {
            foreach(BehaviourNodeData nodeData in behaviourContainer.behaviourNodes)
            {
                if (nodeData.childGuids == null)
                    continue;

                BehaviourGraphBaseNodeUnspecified graphNode = GetNodeByGUID(nodeData.guid);

                foreach (string childGUID in nodeData.childGuids)
                {
                    if (childGUID == "")
                        continue;

                    BehaviourGraphBaseNodeUnspecified childNode = GetNodeByGUID(childGUID);
                    LinkNodes(graphNode.outputPorts.Where((x)=> x.connected == false).First(), childNode.inputPort);
                }
            }
        }

        private BehaviourGraphBaseNodeUnspecified GetNodeByGUID(string requiredGUID)
        {
            return _nodes.Where((x) => x.guid == requiredGUID).First();
        }

        private void AssignDataToNode<T>(T node, BehaviourNodeData nodeData) where T : BehaviourGraphBaseNodeUnspecified
        {
            node.guid = nodeData.guid;
            node.AssignContentFromJSON(nodeData.contentAsJSON);
            node.SetPosition(nodeData.rect);
            _targetGraph.AddElement(node);
        }

        private void ClearGraph()
        {
            if(_nodes == null)
            {
                return;
            }
            foreach(var node in _nodes)
            {
        
                _edges.Where((x) => x.input.node == node).ToList().
                    ForEach(edge => _targetGraph.RemoveElement(edge));
        
                _targetGraph.RemoveElement(node);
            }
        }
    }
}