﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BehaviourEditorColorScheme 
{
    // Background
    public Color background         = new Color(1f, 1f, 1f, 1f);

    // Lines
    public Color nodeLines          = new Color(1f, 1f, 1f, 1f);

    // Node status
    public Color nodeSuccessful     = new Color(1f, 1f, 1f, 1f);
    public Color nodeFailed         = new Color(1f, 1f, 1f, 1f);
    public Color nodeInactive       = new Color(1f, 1f, 1f, 1f);
    public Color nodeRunning        = new Color(1f, 1f, 1f, 1f);

    // Font color
    public Color fontColor          = new Color(1f, 1f, 1f, 1f);

    // Node Colors
    public Color baseNodeColor      = new Color(1f, 1f, 1f, 1f);
    public Color rootNode           = new Color(1f, 1f, 1f, 1f);
    public Color selectorNode       = new Color(1f, 1f, 1f, 1f);
    public Color sequenceNode       = new Color(1f, 1f, 1f, 1f);
    public Color actionNode         = new Color(1f, 1f, 1f, 1f);
    public Color emotionalNode      = new Color(1f, 1f, 1f, 1f);
    public Color decoratorNode      = new Color(1f, 1f, 1f, 1f);

    //Selection window
    public Color selectorColor      = new Color(1f, 1f, 1f, 1f);   

}
