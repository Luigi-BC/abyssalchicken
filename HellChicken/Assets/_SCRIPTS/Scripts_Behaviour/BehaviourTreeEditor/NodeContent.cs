﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AbyssalChicken.Behaviour.EmotionSystem;

namespace AbyssalChicken.Behaviour
{
    public abstract class BehaviourNodeContent
    {
        
    }

    public class ActionNodeContent : BehaviourNodeContent
    {
        public ActionState actionState;

    }

    public class EmotionalConditionNodeContent : BehaviourNodeContent
    {
        public EmotionalState emotion;
    }

    public enum DecoratorType 
    {
        Inverter
    }
    public class DecoratorNodeContent : BehaviourNodeContent
    {
        public DecoratorType decoratorType;
    }

    public class RootGraphNodeContent : BehaviourNodeContent
    {
        
    }

    public class SelectorGraphNodeContent : BehaviourNodeContent
    {
        public int amountOfOutputPorts;
    }

    public class SequenceGraphNodeContent : BehaviourNodeContent
    {
        public int amountOfOutputPorts;
    }
}
