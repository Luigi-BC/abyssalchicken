﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbyssalChicken.Behaviour.EditorGraph
{

    [CreateAssetMenu(fileName = "BehaviourEditorColorSchemeHolderSO", menuName = "Editor/BehaviourTree/BehaviourEditorColorSchemeHolderSO")]
    public class BehaviourEditorColorSchemeHolderSO : ScriptableObject
    {
        public BehaviourEditorColorScheme darkTheme;
        public BehaviourEditorColorScheme lightTheme;

        

    }
}