﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace AbyssalChicken.Behaviour.EditorGraph
{

    
    public abstract class BehaviourGraphBaseNodeUnspecified : Node
    {
        public Action EventRequestColorRefresh = delegate {  };
        
        private Port _inputPort;
        public Port inputPort
        {
            set
            {
                _inputPort = value;
            }
            get { return _inputPort; }
        }
        public List<Port> outputPorts = new List<Port>();

        public string guid;
        public bool entryPoint = false;
        public NodeType nodeType;

        public virtual void Initialize()
        {

        }
        public abstract string GetContentAsJSON();
        public abstract void AssignContentFromJSON(string jsonData);

        public BehaviourGraphBaseNodeUnspecified()
        {
            
        }

        public virtual void LinkGraphNodeToTreeNode(BehaviourNode treeNode)
        {
            ChickenBehaviourTree.Instance.EventResetTreeGraph += OnResetTreeGraph;
            treeNode.EventNodeStateChanged += OnTreeNodeStateChanged;
        }

        private void OnResetTreeGraph()
        {
            OnTreeNodeStateChanged(NodeState.INACTIVE);
        }

        private void OnTreeNodeStateChanged(NodeState state)
        {
            switch (state)
            {
                case NodeState.INACTIVE:
                    //TODO: USE CORRECT COLORS
                    mainContainer.style.borderTopColor = Color.black;
                    mainContainer.style.borderTopWidth = 3f;
                    break;
                case NodeState.RUNNING:
                    mainContainer.style.borderTopColor = Color.yellow;
                    mainContainer.style.borderTopWidth = 3f;
                    break;
                case NodeState.FAILED:
                    mainContainer.style.borderTopColor = Color.red;
                    mainContainer.style.borderTopWidth = 3f;
                    break;
                case NodeState.SUCCESS:
                    mainContainer.style.borderTopColor = Color.green;
                    mainContainer.style.borderTopWidth = 3f;
                    break;
            }
        }

        public virtual void SetEnumFieldColor(Color baseNodeColor, Color fontColor)
        {
            
        }
    }
    /// <summary>
    /// Default Node
    /// </summary>
    public abstract class BehaviourGraphBaseNode<TContentData> : BehaviourGraphBaseNodeUnspecified where TContentData : BehaviourNodeContent
    {
        public TContentData content;

        public override void Initialize()
        {
            base.Initialize();
            CreateNodeContent();
        }

        protected virtual void CreateNodeContent()
        {

        }



        /// <summary>
        /// Default output port
        /// </summary>
        public virtual void AddOutputPort(bool deletable)
        {
            Port outputPort = GeneratePort("", Direction.Output, Port.Capacity.Single);

            //Add delete button
            if (deletable)
            {
                //create button to delete port
                Button button = new Button(clickEvent: () =>
                {
                    List<Edge> connectedEdges = outputPort.connections.ToList();

                    foreach (Edge edge in connectedEdges)
                    {
                        edge.output.Disconnect(edge);
                        edge.input.Disconnect(edge);
                    }

                    while (connectedEdges.Count > 0)
                    {
                        connectedEdges[0].RemoveFromHierarchy();
                        connectedEdges.RemoveAt(0);
                    }

                    outputPort.RemoveFromHierarchy();
                    outputPorts.Remove(outputPort);

                    UpdateOutputPorts();
                });
                button.text = "-";
                button.AddToClassList("portDeleteButton");
                outputPort.Add(button);
                UpdateOutputPorts();
            }

            outputContainer.Add(outputPort);
            RefreshExpandedState();
            RefreshPorts();
            EventRequestColorRefresh();
        }

        protected virtual void UpdateOutputPorts()
        {
            for (int i = 0; i < outputPorts.Count; i++)
            {
                outputPorts[i].portName = (i + 1).ToString();
            }
        }

        /// <summary>
        /// Default input port
        /// </summary>
        public virtual void AddInputPort()
        {
            inputPort = GeneratePort("In", Direction.Input, Port.Capacity.Multi);
            inputContainer.Add(inputPort);
            RefreshExpandedState();
            RefreshPorts();
        }

        /// <summary>
        /// Generates a port with specified values.
        /// </summary>
        protected Port GeneratePort(string portName, Direction portDirection, Port.Capacity capacity = Port.Capacity.Single)
        {

            Port port = InstantiatePort(Orientation.Horizontal, portDirection, capacity, typeof(float));
            port.portName = portName;
            //port.portColor = new Color(0.8f, 0.4f, 0.3f);

            if (portDirection == Direction.Input)
            {
                inputPort = port;
            }
            else
            {
                outputPorts.Add(port);
            }

            return port;
        }

        protected abstract void ApplyContentData();


        public override string GetContentAsJSON()
        {
            return JsonUtility.ToJson(content);
        }


        public override void AssignContentFromJSON(string jsonData)
        {
            content = JsonUtility.FromJson<TContentData>(jsonData);
            ApplyContentData();
        }

    }

}