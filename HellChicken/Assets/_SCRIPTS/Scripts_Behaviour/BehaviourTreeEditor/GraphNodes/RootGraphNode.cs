﻿using UnityEngine;
using System.Collections;
using UnityEditor.Experimental.GraphView;

namespace AbyssalChicken.Behaviour.EditorGraph
{


    public class RootGraphNode : BehaviourGraphBaseNode<RootGraphNodeContent>
    {   
        

        public override void Initialize()
        {
            base.Initialize();
            nodeType = NodeType.Root;
            AddOutputPort(false);
        }

        public override void AddOutputPort(bool deletable)
        {
            Port outputPort = GeneratePort("Start", Direction.Output, Port.Capacity.Single);
            outputContainer.Add(outputPort);
            RefreshExpandedState();
            RefreshPorts();
        }

        protected override void ApplyContentData()
        {

        }
    }
}