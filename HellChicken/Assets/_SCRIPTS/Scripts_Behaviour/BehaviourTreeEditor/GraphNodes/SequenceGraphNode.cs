﻿using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using System.Linq;
using System.Collections.Generic;

namespace AbyssalChicken.Behaviour.EditorGraph
{


    public class SequenceGraphNode : BehaviourGraphBaseNode<SequenceGraphNodeContent>
    {
        private int amountOfOutputPorts;


        public override void Initialize()
        {
            base.Initialize();
            nodeType = NodeType.Sequence;

            
            content = new SequenceGraphNodeContent();
            AddInputPort();
            AddOutputPort(true);

            CreateAddPortButton();
        }

        private void CreateAddPortButton()
        {
            Button button = new Button(clickEvent: () =>
            {
                AddOutputPort(true);
            });

            button.text = "+";
            titleContainer.Add(button);
        }

        public override void AddInputPort()
        {
            base.AddInputPort();
        }

        public override void AddOutputPort(bool deletable)
        {
            amountOfOutputPorts++;
            content.amountOfOutputPorts = amountOfOutputPorts;

            base.AddOutputPort(deletable);
        }


        protected override void ApplyContentData()
        {
            int amountOfPortsNeeded = content.amountOfOutputPorts;

            //Generate enough output ports
            while (amountOfOutputPorts < amountOfPortsNeeded)
            {
                AddOutputPort(true);
            }
        }

        protected override void UpdateOutputPorts()
        {
            base.UpdateOutputPorts();

            amountOfOutputPorts = outputPorts.Count;
            content.amountOfOutputPorts = amountOfOutputPorts;

        }

    }
}