﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace AbyssalChicken.Behaviour.EditorGraph
{
    public class DecoratorGraphNode : BehaviourGraphBaseNode<DecoratorNodeContent>
    {
        /// <summary>
        /// Enum field for the currently active decorator type
        /// </summary>
        private EnumField _enumField;
        
        
        public override void Initialize()
        {

            nodeType = NodeType.Decorator;
            content = new DecoratorNodeContent();
            
            AddInputPort();
            AddOutputPort(false);

            CreateNodeContent();
        }

        public override void SetEnumFieldColor(Color baseNodeColor, Color fontColor)
        {
            _enumField.Children().First().style.backgroundColor = baseNodeColor;
            _enumField.Children().First().Children().First().style.color = fontColor;
        }

        
        protected override void ApplyContentData()
        {
            _enumField.SetValueWithoutNotify(content.decoratorType);
        }

        protected override void CreateNodeContent()
        {
            _enumField = new EnumField(content.decoratorType);
            _enumField.RegisterValueChangedCallback(OnEnumChanged);
            extensionContainer.Add(_enumField);
            RefreshExpandedState();
        }

        private void OnEnumChanged(ChangeEvent<Enum> evt)
        {
            content.decoratorType = (DecoratorType) evt.newValue;
        }
        
    }
}