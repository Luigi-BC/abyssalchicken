﻿using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System;
using System.Linq;

namespace AbyssalChicken.Behaviour.EditorGraph
{

    public class ActionGraphNode : BehaviourGraphBaseNode<ActionNodeContent>
    {
        private EnumField _enumField;

        public override void Initialize()
        {
            //Color 
            nodeType = NodeType.ActionNode;

            content = new ActionNodeContent();

            CreateNodeContent();
            AddInputPort();
        }

        protected override void ApplyContentData()
        {
            _enumField.SetValueWithoutNotify(content.actionState);            
        }

        protected override void CreateNodeContent()
        {
            _enumField = new EnumField(content.actionState);

            _enumField.RegisterValueChangedCallback(OnEnumValueChanged);

            extensionContainer.Add(_enumField);
            RefreshExpandedState();
        }

        public override void SetEnumFieldColor(Color baseNodeColor, Color fontColor)
        {
            _enumField.Children().First().style.backgroundColor = baseNodeColor;
            _enumField.Children().First().Children().First().style.color = fontColor;
        }

        private void OnEnumValueChanged(ChangeEvent<Enum> evt)
        {
            content.actionState = (ActionState)evt.newValue;
        }
    }
}
