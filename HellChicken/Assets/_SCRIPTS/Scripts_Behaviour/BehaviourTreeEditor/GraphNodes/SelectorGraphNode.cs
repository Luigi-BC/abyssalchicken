﻿using UnityEngine;
using System.Collections;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace AbyssalChicken.Behaviour.EditorGraph
{


    public class SelectorGraphNode : BehaviourGraphBaseNode<SelectorGraphNodeContent>
    {

        private int amountOfOutputPorts;

        public override void Initialize()
        {
            base.Initialize();
            nodeType = NodeType.Selector;


            content = new SelectorGraphNodeContent();
            AddInputPort();
            AddOutputPort(true);

            CreateAddPortButton();
        }

        private void CreateAddPortButton()
        {
            Button button = new Button(clickEvent: () =>
            {
                AddOutputPort(true);
            });

            button.text = "+";
            titleContainer.Add(button);
        }

        protected override void ApplyContentData()
        {
            int amountOfPortsNeeded = content.amountOfOutputPorts;
            
            //Generate enough output ports
            while(amountOfOutputPorts < amountOfPortsNeeded)
            {
                AddOutputPort(true);
            }
        }

        public override void AddOutputPort(bool deletable)
        {
            amountOfOutputPorts++;
            content.amountOfOutputPorts = amountOfOutputPorts;
            base.AddOutputPort(deletable);
        }
    }
}