﻿using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System;
using System.Linq;
using AbyssalChicken.Behaviour.EmotionSystem;

namespace AbyssalChicken.Behaviour.EditorGraph
{


    public class ConditionGraphNode_Emotion : BehaviourGraphBaseNode<EmotionalConditionNodeContent>
    {
        private EnumField _enumField;


        public override void Initialize()
        {
            //Node Color
            nodeType = NodeType.EmotionalCondition;
            content = new EmotionalConditionNodeContent();
            //Input port
            AddInputPort();

            CreateNodeContent();
        }

        protected override void ApplyContentData()
        {
            _enumField.SetValueWithoutNotify(content.emotion);
        }

        public override void SetEnumFieldColor(Color baseNodeColor, Color fontColor)
        {
            _enumField.Children().First().style.backgroundColor = baseNodeColor;
            _enumField.Children().First().Children().First().style.color = fontColor;
        }

        protected override void CreateNodeContent()
        {
            _enumField = new EnumField(content.emotion);
            _enumField.RegisterValueChangedCallback(OnEnumChanged);
            extensionContainer.Add(_enumField);
            RefreshExpandedState();
        }

        private void OnEnumChanged(ChangeEvent<Enum> evt)
        {
            content.emotion = (EmotionalState) evt.newValue;
        }
    }
}