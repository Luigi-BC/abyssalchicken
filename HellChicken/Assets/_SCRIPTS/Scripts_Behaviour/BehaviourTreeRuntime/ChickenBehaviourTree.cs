﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AbyssalChicken.Behaviour
{
    /// <summary>
    /// Class that holds the rootnode and traverses the tree.
    /// </summary>
    public class ChickenBehaviourTree : MonoBehaviour
    {
        // cache for the save data of the tree
        [SerializeField] private BehaviourContainer _container;

        private RootNode _rootNode;

#if UNITY_EDITOR
        public static ChickenBehaviourTree Instance;
        public Action EventResetTreeGraph = delegate { };
        public List<BehaviourNode> allNodes;
#endif


        private void Awake()
        {
#if UNITY_EDITOR
            allNodes = new List<BehaviourNode>();
            Instance = this;
#endif
            LoadBehaviour();
        }


        public void Update()
        {
#if UNITY_EDITOR
            EventResetTreeGraph();
#endif
            _rootNode.Tick();
        }


        private void LoadBehaviour()
        {
            // todo: maybe load the tree via Resources and not as a SerializeField 
            // TODO: DEFINITELY LOAD VIA RESOURCES, fucking thing loses reference everytime you save
            // (reference sometimes gets destroyed when saving the SO, not sure why)
            BehaviourNodeData rootNodeData = _container.behaviourNodes.First(x => x.nodeType == NodeType.Root);
            _rootNode = CreateNodeFromNodeData<RootNode>(rootNodeData);
        }

        /// <summary>
        /// Creates a node from the data and also for its children (recursive)
        /// </summary>
        private T CreateNodeFromNodeData<T>(BehaviourNodeData data) where T : BehaviourNode, new()
        {
            //Create a new node
            T newNode = new T();
            newNode.guid = data.guid;
            newNode.childNodes = new List<BehaviourNode>();

            //Return node if there are no children to this node
            if(data.childGuids == null)
            {
                return newNode;
            }

            //Create children nodes
            for(int i = 0; i < data.childGuids.Count; i++)
            {
                BehaviourNodeData childData = _container.behaviourNodes.First(x => x.guid == data.childGuids[i]);

                BehaviourNode childNode = null; 

                switch (childData.nodeType)
                {
                    case NodeType.Root:
                        childNode = CreateNodeFromNodeData<RootNode>(childData);
                        break;
                    case NodeType.Selector:
                        childNode = CreateNodeFromNodeData<SelectorNode>(childData);
                        break;
                    case NodeType.Sequence:
                        childNode = CreateNodeFromNodeData<SequenceNode>(childData);
                        break;
                    case NodeType.EmotionalCondition:
                        childNode = CreateNodeFromNodeData<EmotionalConditionNode>(childData);
                        break;
                    case NodeType.ActionNode:
                        childNode = CreateNodeFromNodeData<ActionNode>(childData);
                        break;
                    case NodeType.Decorator:
                        childNode = CreateNodeFromNodeData<DecoratorNode>(childData);
                        break;
                }
                childNode.AssignContent(childData.contentAsJSON);

                newNode.childNodes.Add(childNode);
            }

            allNodes.Add(newNode);
            return newNode;
        }
    }
}