﻿using UnityEngine;
using System.Collections;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class SequenceNode : BehaviourNode
    {
        public override void AssignContent(string contentAsJSON)
        {

        }

        protected override NodeState TickInternal()
        {
            bool allSuccessful = true;

            for (int i = 0; i < childNodes.Count; i++)
            {
                NodeState childState = childNodes[i].Tick();
                if (childState == NodeState.FAILED)
                    return NodeState.FAILED;

                if (childState == NodeState.RUNNING)
                {
                    allSuccessful = false;
                }
            }   
             
            if (allSuccessful)
            {
                 return NodeState.SUCCESS;
            }
            else 
            {
                return NodeState.RUNNING; 
            }
        }
    }
} 