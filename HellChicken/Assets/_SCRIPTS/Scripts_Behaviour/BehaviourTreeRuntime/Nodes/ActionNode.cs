﻿using UnityEngine;
using System.Collections;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class ActionNode : BehaviourNode
    {
        private ActionState _action;

        public override void AssignContent(string contentAsJSON)
        {
            ActionNodeContent content = JsonUtility.FromJson<ActionNodeContent>(contentAsJSON);
            _action = content.actionState;
        }

        protected override NodeState TickInternal()
        {
            BehaviourTest.instance.Currentaction = _action;
            //Returns true because an Action Node (for now) can always be executed
            return NodeState.SUCCESS;
        }
        
    }
}