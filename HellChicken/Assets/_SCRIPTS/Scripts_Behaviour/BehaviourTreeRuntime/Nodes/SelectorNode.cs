﻿using UnityEngine;
using System.Collections;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class SelectorNode : BehaviourNode
    {
        public override void AssignContent(string contentAsJSON)
        {

        }

        protected override NodeState TickInternal()
        {
            bool allFailed = true;

            for (int i = 0; i < childNodes.Count; i++)
            {
                NodeState childState = childNodes[i].Tick();
                if (childState == NodeState.SUCCESS)
                    return NodeState.SUCCESS;

                if (childState == NodeState.RUNNING)
                {
                    allFailed = false;
                }
            }

            if (allFailed)
            {
                return NodeState.FAILED;
            }
            else
            {
                return NodeState.RUNNING;
            }
        }
    }
}
