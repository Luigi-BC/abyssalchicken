﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbyssalChicken.Behaviour
{

    [System.Serializable]   
    public abstract class BehaviourNode 
    {
        public string guid;
        public Action<NodeState> EventNodeStateChanged = delegate { };
        public List<BehaviourNode> childNodes;

        private NodeState _currentNodeState;



        public BehaviourNode()
        { 
             
        }

        public abstract void AssignContent(string contentAsJSON);

        protected abstract NodeState TickInternal();
        
        public NodeState Tick()
        {
            _currentNodeState = TickInternal();
#if UNITY_EDITOR
            //for debugging only (probably)
            EventNodeStateChanged(_currentNodeState);
#endif
            return _currentNodeState;
        }

    }

    public enum NodeState
    {
        INACTIVE,
        RUNNING,
        FAILED,
        SUCCESS
    }
}