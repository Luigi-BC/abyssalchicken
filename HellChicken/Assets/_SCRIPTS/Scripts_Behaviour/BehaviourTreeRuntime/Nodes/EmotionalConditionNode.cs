﻿using UnityEngine;
using System.Collections;
using AbyssalChicken.Behaviour.EmotionSystem;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class EmotionalConditionNode : BehaviourNode
    {
        private EmotionalState _emotion;

        public override void AssignContent(string contentAsJSON)
        {
            EmotionalConditionNodeContent content = JsonUtility.FromJson<EmotionalConditionNodeContent>(contentAsJSON);
            _emotion = content.emotion;
        }

        protected override NodeState TickInternal()
        {
            //TODO: JUST FOR TESTING
            //This only tests the Testbehaviourtrees emotion
            if(BehaviourTest.instance.currentEmotion == _emotion)
            {
                return NodeState.SUCCESS;
            }
            else
            {
                return NodeState.FAILED;
            }

        }
    }
}