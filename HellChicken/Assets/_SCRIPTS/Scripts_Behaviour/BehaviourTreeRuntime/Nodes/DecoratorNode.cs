﻿using System;
using System.Collections;
using System.Collections.Generic;
using AbyssalChicken.Behaviour.EditorGraph;
using UnityEngine;

namespace AbyssalChicken.Behaviour
{
    public class DecoratorNode : BehaviourNode
    {
        private DecoratorType _decoratorType;
        
        
        public override void AssignContent(string contentAsJSON)
        {
            DecoratorNodeContent content = JsonUtility.FromJson<DecoratorNodeContent>(contentAsJSON);
            _decoratorType = content.decoratorType;
        }

        protected override NodeState TickInternal()
        {
            switch (_decoratorType)
            {
                case DecoratorType.Inverter:
                    return HandleInverterTick();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Node Behaviour if selected Decorator Type is Inverter
        /// </summary>
        private NodeState HandleInverterTick()
        {
            NodeState childState = childNodes[0].Tick();
            switch (childState)
            {
                case NodeState.INACTIVE:
                    return NodeState.INACTIVE;
                case NodeState.RUNNING:
                    return NodeState.RUNNING;
                case NodeState.FAILED:
                    return NodeState.SUCCESS;
                case NodeState.SUCCESS:
                    return NodeState.FAILED;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}