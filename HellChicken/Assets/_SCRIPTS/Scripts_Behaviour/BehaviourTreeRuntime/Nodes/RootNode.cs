﻿using UnityEngine;
using UnityEditor;

namespace AbyssalChicken.Behaviour
{
    [System.Serializable]
    public class RootNode : BehaviourNode
    {
        public override void AssignContent(string contentAsJSON)
        {

        }

        protected override NodeState TickInternal()
        {
            Debug.Log("Traversing Tree");
            childNodes[0].Tick();
            return NodeState.SUCCESS;
        }
    }
}